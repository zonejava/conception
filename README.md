# Zone Java project - Overview
## Link
You can access to the application at https://www.zone-java.fr .
## Description
The Zone Java project is a java tutorials share application. I note there is not a lot of french source when you 
are beginner. I use to search simple tutorial when i start my developer cursus and english sources i found were not
always easy to understand. Of course it's important to work in english but when you are beginner it's a second 
difficulty that i think it should come later.

In a second time i want to implement a questions/answers forum like the famous [stackoverflow](https://stackoverflow.com/).
Again, in french!

The project architecture for the v1 : 
![](ZoneJava-architectureDiagram.jpg)

## Installation & Run
You can download and run the all project with docker [following this link]() or you can run each API separatly following
procedures in README.md of repositories (choose master branch) :
* [Front Angular](https://gitlab.com/zonejava/zj-webappangular)
* [Spring Gateway](https://gitlab.com/zonejava/zj-gatewayserver)
* [Eureka Discovery Server](https://gitlab.com/zonejava/zj-discoveryserver)
* [Spring Cloud Server](https://gitlab.com/zonejava/zj-configserver)
* [Spring Boot Monitoring](https://gitlab.com/zonejava/zj-monitoringserver)
* [Account Management](https://gitlab.com/zonejava/zj-accountmanagement)
* [Tutorial Management](https://gitlab.com/zonejava/zj-tutorielmanagement)
* [Commentary Management](https://gitlab.com/zonejava/zj-commentarymanagement)

You need your own [Keycloak server](https://www.keycloak.org/) and inquire required configuration in each management 
services and angular application like described in their README. If you just want to test, you can skip this part but even
if you could create account and login, you could not manage your created account.
## Participation
Feel free to join the project. More features are comming and any help will be welcome.
## Comming features
* forum : Q/A
* Tutorial rating
* Tags on tutorial and post to improve search results
* Contributer popularity 
## Owner
Arnaud Laval - arnaudlaval33@gmail.com

The Zone Java project is developed as part of final project of DA Java cursus (OpenClassrooms)